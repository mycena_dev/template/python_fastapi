from tables import Unknown
from pydantic import BaseModel
from enum import Enum
from typing import List

class SafetyItemState(str, Enum):
    UNKNOWN = "Unknown"
    TRUE = "True"
    FALSE = "False"


class SafetyItem(BaseModel):
    safetyItemId: str
    aiPridectState: SafetyItemState
    manualState: SafetyItemState
    class Config:  
        use_enum_values = True

class SafetyItemDic(BaseModel):
    environment: List[SafetyItem] = []
    operation: List[SafetyItem] = []
    equipment: List[SafetyItem] = []
    personalProtectiveEquipment: List[SafetyItem] = []
    

class SafetyAuditContent(BaseModel):
    videoId: str
    scence: str
    safetyItemDic: SafetyItemDic