from fastapi import FastAPI
import uvicorn
from controllers import safety_audit_controller, media_controller, root_web_controller
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles

app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

root_web_controller.init_static_web_conf(app=app)
app.include_router(root_web_controller.router)
app.include_router(safety_audit_controller.router)
app.include_router(media_controller.router)

if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=3000, reload=True)