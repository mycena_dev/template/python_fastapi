FROM python:3.9.7-slim-buster

WORKDIR /work

COPY . /work

RUN pip3 install -r requirements.txt

RUN pip3 install "uvicorn[standard]"

RUN pip3 install Jinja2 --upgrade

EXPOSE 3000

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "3000"]
