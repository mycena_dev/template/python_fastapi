from domains.safety_audit_content import SafetyAuditContent, SafetyItem
from fastapi import APIRouter, HTTPException
from services.safety_audit_service import SafetyAuditService

router: APIRouter = APIRouter(
    prefix="/safety-audit",
    tags=["safety-audit"],
)

@router.get('/review/{videoId}', response_model=SafetyAuditContent)
async def get_safety_audit_content(videoId: str):
    result: SafetyAuditContent = SafetyAuditService.get_one_by_video_id(videoId)
    if result  is None:
        raise HTTPException(status_code=404, detail="Item not found.")
    return result

@router.put('/review', response_model=SafetyAuditContent)
async def put_safety_audit_content(safetyAuditContent: SafetyAuditContent):
    result: SafetyAuditContent = SafetyAuditService.upsert_one(safety_audit=safetyAuditContent)
    if result is None:
        raise HTTPException(status_code=500, detail="Upsert data failed.")
    return result