from pathlib import Path
from fastapi import Header, Response, APIRouter

router: APIRouter = APIRouter(
    prefix="/video",
    tags=["video"],
)

CHUNK_SIZE = int(1024*1024*2)

@router.get("/{video_id}")
async def video_endpoint(video_id: str, range: str = Header(None)):
    start, _ = range.replace("bytes=", "").split("-")
    start = int(start)
    end = start + CHUNK_SIZE
    file_path = f'static/{video_id}.mp4'
    video_path = Path(file_path)
    with open(video_path, "rb") as video:
        filesize = str(video_path.stat().st_size)
        end = video_path.stat().st_size - 1 if end > video_path.stat().st_size else end
        size = end - start + 1
        video.seek(start)
        data = video.read(end - start + 1)
        headers = {
            "Content-Type": "video/mp4",
            "Accept-Ranges": "bytes",
            "Content-Encoding": "identity",
            "Content-Length": str(size),
            "Access-Control-Expose-Headers": (
                "Content-Type, Accept-Ranges, Content-Length, Content-Range, Content-Encoding"
            ),
        }
        headers["Content-Range"] =  f'bytes {str(start)}-{str(end)}/{filesize}'
        
        return Response(data, status_code=206, headers=headers, media_type="video/mp4")