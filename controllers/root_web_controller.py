from fastapi import FastAPI, Request
from pathlib import Path
from fastapi import Header, Response, APIRouter
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from starlette.responses import FileResponse

templates = Jinja2Templates(directory="dist")

router: APIRouter = APIRouter(
    prefix="",
    tags=["web"],
)

def init_static_web_conf(app: FastAPI):
    app.mount("/_app", StaticFiles(directory="dist/_app", html = True), name="_app")

@router.get("/")
async def example(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})

@router.get("/safety-audit")
async def example(request: Request):
    return templates.TemplateResponse("safety-audit.html", {"request": request})

@router.get('/favicon.png')
async def favicon():
    return FileResponse('dist/favicon.png')