from tinydb import TinyDB
from tinydb.table import Table

class DBAgent:
    __PATH: str = "./system.json"

    __db: TinyDB = TinyDB(path=__PATH)

    safety_audit_collection: Table = __db.table('safety')

