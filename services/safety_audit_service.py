from agents.nosql_agent import DBAgent
from tinydb import Query
from domains.safety_audit_content import SafetyAuditContent

class SafetyAuditService:
    data_agent = DBAgent.safety_audit_collection

    @classmethod
    def get_one_by_video_id(cls, safety_audit_video_id) -> SafetyAuditContent:
        safety_audit_query = Query()
        safety_audit = cls.data_agent.search(safety_audit_query.videoId == safety_audit_video_id)
        try:
            return SafetyAuditContent.parse_obj(safety_audit[0])
        except:
            return None

    @classmethod
    def upsert_one(cls, safety_audit : SafetyAuditContent) -> SafetyAuditContent:

        current_data: SafetyAuditContent = SafetyAuditService.get_one_by_video_id(safety_audit.videoId)
        for index1, v1 in enumerate(current_data.safetyItemDic.environment):
            for index2, v2 in enumerate(safety_audit.safetyItemDic.environment):
                if v1.safetyItemId == v2.safetyItemId:
                    current_data.safetyItemDic.environment[index1] = safety_audit.safetyItemDic.environment[index2]

        for index1, v1 in enumerate(current_data.safetyItemDic.operation):
            for index2, v2 in enumerate(safety_audit.safetyItemDic.operation):
                if v1.safetyItemId == v2.safetyItemId:
                    current_data.safetyItemDic.operation[index1] = safety_audit.safetyItemDic.operation[index2]

        for index1, v1 in enumerate(current_data.safetyItemDic.equipment):
            for index2, v2 in enumerate(safety_audit.safetyItemDic.equipment):
                if v1.safetyItemId == v2.safetyItemId:
                    current_data.safetyItemDic.equipment[index1] = safety_audit.safetyItemDic.equipment[index2]

        for index1, v1 in enumerate(current_data.safetyItemDic.personalProtectiveEquipment):
            for index2, v2 in enumerate(safety_audit.safetyItemDic.personalProtectiveEquipment):
                if v1.safetyItemId == v2.safetyItemId:
                    current_data.safetyItemDic.personalProtectiveEquipment[index1] = safety_audit.safetyItemDic.personalProtectiveEquipment[index2]

        current_data.scence = safety_audit.scence

        safety_audit_query = Query()
        state_num = cls.data_agent.upsert(current_data.dict() ,safety_audit_query.videoId == current_data.videoId)
        if state_num[0] >= 0:
            return current_data
        return None